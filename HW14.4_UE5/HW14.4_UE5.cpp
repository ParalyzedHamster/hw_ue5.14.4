﻿#include <iostream>
#include <stdint.h>
#include <iomanip>
#include <string>

int main()
{
    //std::cout << "Enter your name!\n";
    //std::string name;
    //std::getline (std::cin , name);
    //
    //std::cout << "Enter your age!\n";
    //std::string age;
    //std::getline(std::cin, age);

    //std::cout << name << " " << age <<"\n";
    std::string x = "Daniil";
    std::string y = " Vashakidze";

    std::cout << x + y << "\n";

    std::cout << x.length() << "\n";
    std::cout << y.length() << "\n";

    std::cout << (x.length() > 1 ? x.substr(0, 1) + x.substr(x.length() - 1, 1) : std::string(x.length(), x.front()));
    std::cout << (y.length() > 1 ? y.substr(1, 1) + y.substr(y.length() - 1, 1) : std::string(y.length(), y.front()));

    std::cin;
    return 0;
}